
fn getchar() -> u8 {
    let mut line = String::new();
    std::io::stdin().read_line(&mut line).unwrap();
    
    return line.chars().nth(0).unwrap() as u8
}

const MAX: usize = 30_000;

fn incr_pointer(pointer: usize) -> usize {
    if pointer == MAX - 1 {
        panic!("error pointer goes upper than `{}`", MAX);
    }
    return pointer + 1
}

fn decr_pointer(pointer: usize) -> usize {
    if pointer == 0 {
        panic!("error pointer goes lower than `0`");
    }
    return pointer - 1
}

fn execute(mut array: &mut Vec<u8>, mut pointer: &mut usize, bf: &Vec<&str>, mut i: usize) -> usize {
    'main: loop {
        if i >= bf.len() { break 'main }
        match bf[i] {
            "-" => array[*pointer] -= 1,
            "+" => array[*pointer] += 1,
            "<" => *pointer = decr_pointer(*pointer),
            ">" => *pointer = incr_pointer(*pointer),
            "." => print!("{}", array[*pointer] as char),
            "," => array[*pointer] = getchar(),
            "]" => break 'main,
            "[" => {
                i += 1; // span the loop start
                let mut next = i;
                'subloop: loop {
                    if array[*pointer] == 0 {
                        break 'subloop;
                    }
                    next = execute(&mut array, &mut pointer, &bf, i);
                }
                i = next;
            },
            _ => {}
        }
        i += 1;
    }
    return i;
}


fn main() {
    let lbf = vec![
        // Hello World!
        "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.",
        
        // hi mom
        "+[----->+++<]>+.+.[--->+<]>---.+[----->+<]>.++.--."
    ];
    
    for _bf in lbf {
        println!("from: `{}`", _bf);

        // convert to vector string
        let mut bf: Vec<&str> = _bf.split("").collect();
        bf.remove(0);
        bf.remove(bf.len() - 1);

        let mut array: Vec<u8> = vec![0;MAX];

        let mut pointer = 0;

        execute(&mut array, &mut pointer, &bf, 0);
        println!("");
    }
}
